package roboclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Main entry point of the application
 */
@SpringBootApplication
public class RoborallyMainClass {
    public static void main(String[] args) {
        SpringApplication.run(RoborallyMainClass.class, args);
    }
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}


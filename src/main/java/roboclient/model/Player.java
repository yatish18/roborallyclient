package roboclient.model;


import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * Model for player
 */
public class Player {
    private String id;
    private String name;
    private String clientURI;
    private Game currentgame;
    private Robot bot;
    private Position startposition;
    private Card[] currentDrawPile;
    private Card[] selectedDrawPile;

    public Player() throws UnknownHostException {
        this.name = "default_player";
        this.clientURI = this.generateClientURI();
        this.currentgame = new Game();
        this.bot = new Robot("Hammer Bot");
        this.startposition = new Position(0, 0);
        this.currentDrawPile = new Card[]{Card.MOVE1, Card.LEFT_TURN, Card.MOVE_BACK, Card.MOVE2, Card.AGAIN, Card.LEFT_TURN, Card.MOVE3, Card.MOVE1, Card.AGAIN};
        this.id = "ABGDGEGA1256";
    }

    public Player(String name, String clientURI) {
        this.name = name;
        this.clientURI = clientURI;
    }

    public Player(String name) throws UnknownHostException {
        this();
        this.name = name;
    }

    public Player(Robot bot, Position startposition) {
        this.bot = bot;
        this.startposition = startposition;
    }

    public Card[] getCurrentDrawPile() {
        return currentDrawPile;
    }

    public Card[] getSelectedDrawPile() {
        return selectedDrawPile;
    }

    public void setSelectedDrawPile(Card[] selectedDrawPile) {
        this.selectedDrawPile = selectedDrawPile;
    }

    public void setCurrentDrawPile(Card[] currentDrawPile) {
        this.currentDrawPile = currentDrawPile;
    }

    public Position getStartPosition() {
        return startposition;
    }

    public void setPosition(Position position) {
        this.startposition = position;
    }

    public Robot getBot() {
        return bot;
    }

    public void setBot(Robot bot) {
        this.bot = bot;
    }

    public Game getCurrentgame() {
        return currentgame;
    }

    public void setCurrentgame(Game currentgame) {
        this.currentgame = currentgame;
    }

    public String getName() {
        return name;
    }

    public String getClientURI() {
        return clientURI;
    }


    private String generateClientURI() throws UnknownHostException {
        String ipAddress = Inet4Address.getLocalHost().getHostAddress();
        return "http://" + ipAddress + "/rest/";

    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", clientURI='" + clientURI + '\'' +
                ", currentgame=" + currentgame +
                '}';
    }
}

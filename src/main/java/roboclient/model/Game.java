package roboclient.model;


import org.apache.log4j.Logger;
import roboclient.controller.http.HTTPGameController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Model for whole game
 **/
public class Game {
    private String name;
    private int maxRoboCount;
    private int currentRoboCount;
    private String id;
    private String status;
    private List<String> players;
    private List<Round> rounds;
    Logger logger = Logger.getLogger(HTTPGameController.class.getName());

    public Game() {
        this.setId("0000");
        this.setCurrentRoboCount(0);
        this.setMaxRoboCount(0);
        this.setName("not_set");
        this.setStatus("NOT_STARTED");
        this.setPlayers(new ArrayList<>());
        this.setRounds(new ArrayList<>());
    }

    public Game(String name, int maxRoboCount, int currentRoboCount, String id) {
        this(name, maxRoboCount);
        this.currentRoboCount = currentRoboCount;
        this.id = id;
    }

    public Game(String name, int maxRoboCount) {
        this();
        this.name = name;
        this.maxRoboCount = maxRoboCount;
    }

    public Game(String name, int maxRoboCount, int currentRoboCount, String id, String status, List<String> players) {
        this.name = name;
        this.maxRoboCount = maxRoboCount;
        this.currentRoboCount = currentRoboCount;
        this.id = id;
        this.status = status;
        this.players = players;
    }

    //TODO
    public State getCurrentStateOfPlayer(Robot bot) {
        Optional<State> player_tmp_state = this.getLatestRound().getStateLog().stream().filter(element -> element.getBot().equals(bot)).skip(this.getLatestRound().getStateLog().stream().filter(element -> element.getBot().equals(bot)).count() - 1).findFirst();
        if (player_tmp_state.isPresent()) {
            return player_tmp_state.get();
        } else {
            logger.info("Oops something went wrong...no State?");
            return null;
            //Better Optional!
        }
    }

    public Round getLatestRound() {
        return this.getRounds().get(this.getRounds().size() - 1);
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxRoboCount() {
        return maxRoboCount;
    }

    public void setMaxRoboCount(int maxRoboCount) {
        this.maxRoboCount = maxRoboCount;
    }

    public int getCurrentRoboCount() {
        return currentRoboCount;
    }

    public void setCurrentRoboCount(int currentRoboCount) {
        this.currentRoboCount = currentRoboCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void addRound(Round round) {
        this.rounds.add(round);
    }
}

package roboclient.model;

/**
 * All the Elements of a board.
 */
public enum Checkpoint {
    CHECK1,
    CHECK2,
    CHECK3,
    CHECK4,
    CHECK5,
    CHECK6
}
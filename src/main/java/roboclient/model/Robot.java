package roboclient.model;

public class Robot {
    private String name;

    public Robot(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Robot robot = (Robot) o;
        return name != null ? name.equals(robot.name) : robot.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public String getName() {
        return name;
    }


}

package roboclient.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds all the rounds in a game.
 */
public class Round {
    private List<State> stateLog;

    public List<State> getStateLog() {
        return stateLog;
    }

    public Round() {
        this.stateLog = new ArrayList<>();
    }

    public Round(List<State> actionLog) {
        this.stateLog = actionLog;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Round round = (Round) o;

        return stateLog != null ? stateLog.equals(round.stateLog) : round.stateLog == null;
    }

    @Override
    public int hashCode() {
        return stateLog != null ? stateLog.hashCode() : 0;
    }
}

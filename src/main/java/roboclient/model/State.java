package roboclient.model;

import java.util.List;

/**
 * Model for each State in the round.
 */
public class State {
    private String playerId;
    private List<Checkpoint> checkpoints;
    private Position position;
    private Robot bot;

    public State(String playerId, List<Checkpoint> checkpoints, Position position, Robot bot) {
        this.playerId = playerId;
        this.checkpoints = checkpoints;
        this.position = position;
        this.bot = bot;
    }

    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    public Position getPosition() {
        return position;
    }

    public Robot getBot() {
        return bot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        State state = (State) o;

        if (checkpoints != null ? !checkpoints.equals(state.checkpoints) : state.checkpoints != null)
            return false;
        return position != null ? position.equals(state.position) : state.position == null;
    }

    @Override
    public int hashCode() {
        int result = playerId != null ? playerId.hashCode() : 0;
        result = 31 * result + (checkpoints != null ? checkpoints.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (bot != null ? bot.hashCode() : 0);
        return result;
    }
}

package roboclient.manager.event;

import org.springframework.context.ApplicationEvent;

/**
 * When Round is ended Master sends its state to player
 * this class will publishes an event to View console to display received state
 */
public class RestEventNewState extends ApplicationEvent {
    public RestEventNewState(Object source) {
        super(source);
    }
}

package roboclient.manager.event;

import org.springframework.context.ApplicationEvent;

/**
 * When Master node sends cards to player
 * this class will publishes an event to View console to display received cards
 */
public class RestEventDrawPile extends ApplicationEvent {
    public RestEventDrawPile(Object source) {
        super(source);
    }
}

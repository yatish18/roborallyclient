package roboclient.manager.event;

import org.springframework.context.ApplicationEvent;

/**
 * When Master node starts game it sends to player
 * this class will publishes an event to View console
 */
public class RestEventStartGame extends ApplicationEvent {
    public RestEventStartGame(Object source) {
        super(source);
    }
}

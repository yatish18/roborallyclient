package roboclient.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import roboclient.controller.http.HTTPGameController;
import roboclient.manager.event.RestEventDrawPile;
import roboclient.manager.event.RestEventNewState;
import roboclient.manager.event.RestEventStartGame;
import roboclient.model.*;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Central controller Class
 * Used for controlling everything in the project, managing user-input, rest-input
 * providing information for ConsoleView, RestController and trigger http-sending-methods.
 * Known Problems: Use here better a Fassade-Pattern to prevent calls right into the model
 * from ConsoleView(example)
 */
@Service
public class GameManager {
    private HTTPGameController httpController;
    private Player me;

    private ApplicationEventPublisher publisher;

    public Player getMe() {
        return me;
    }

    /**
     * Autowire constructor for necessary dependencies (HttpGameController, ApplicationEventPublisher)
     * Known Problems: No proper exception handling...
     *
     * @param controller Used for DP by Spring
     * @param publisher  Used for DP by Spring (EventPublisher)
     * @throws UnknownHostException Forwarded exception from Player object
     */
    @Autowired
    public GameManager(HTTPGameController controller, ApplicationEventPublisher publisher) throws UnknownHostException {
        httpController = controller;
        this.me = new Player();
        this.publisher = publisher;
    }

    /**
     * Method for creating a new Game (Setting Game in Model, and send request to Game Master)
     * Known Problems: What happens if the httpController not returning a valid Game object?
     *
     * @param gameName      Name of the game
     * @param maxRobotCount Max Robot count of the game
     */
    public void createGame(String gameName, int maxRobotCount) {
        this.me.setCurrentgame(httpController.createGame(new Game(gameName, maxRobotCount)));
    }

    /**
     * Match the choosen registeres to the corresponding Card values
     * and send them via httpController to Game Master
     *
     * @param indexReturn Array containing the index of the choosen Cards
     * @return Return true if sending data was successful, otherwise false
     */
    public boolean mapAndSendRegisters(int[] indexReturn) {
        Card[] cards = this.mapIndexToRegisters(indexReturn);
        this.getMe().setSelectedDrawPile(cards);
        return httpController.sendRegistersToMaster(this.getMe().getCurrentgame().getId(), cards);
    }

    /**
     * Helper Method to match Card indexes to Cards
     *
     * @param index Array of indexes
     * @return Corresponding Card Array
     */
    private Card[] mapIndexToRegisters(int[] index) {
        return Arrays.stream(index).mapToObj(s -> Card.valueOf(this.getMe().getCurrentDrawPile()[s].name())).collect(Collectors.toList()).toArray(new Card[index.length]);
    }


    /**
     * Method with Fassade-Pattern implementation to hide getter calls of model
     *
     * @return Name of the currentGame as a String
     */
    public String getCurrentGameName() {
        return this.me.getCurrentgame().getName();
    }

    /**
     * Event-Method to inform ConsoleView that a new draw pile was sent
     */
    public void informNewDrawPile() {
        publisher.publishEvent(new RestEventDrawPile(this));
    }

    /**
     * Event-Method inform ConsoleView about a new changed state of the players bot position
     */
    public void informNewState() {
        publisher.publishEvent(new RestEventNewState(this));
    }

    /**
     * Event-Method inform ConsoleView that the game was started
     */
    public void informStartGame() {
        publisher.publishEvent(new RestEventStartGame(this));
    }

    /**
     * Used to create a new Player
     * Known Problems: No proper exception handling
     *
     * @param name Name of player
     * @throws UnknownHostException Forwarded exception from Player object
     */
    public void enterPlayerName(String name) throws UnknownHostException {
        this.me = new Player(name);
    }

    /**
     * Method to add a new Round to the current game, if round actions are received through RestController
     * Known Problems: No return type?...
     *
     * @param round New round you want to add to the list of played rounds
     */
    public void newRound(Round round) {
        this.getMe().getCurrentgame().addRound(round);
        this.informNewState();
    }

    /**
     * Method to exit a game properly
     *
     * @return returns true if exiting the game was successful otherwise false
     */
    public boolean exitGame() {
        return httpController.exitGameToMaster(this.getMe().getCurrentgame().getId());
    }


    /**
     * Set current drawPile to make it available in the View(ConsoleView)
     * ConsoleView will be informed about the new pile via Event
     * Known Problems: No return type?
     *
     * @param drawPile New Pile you want to store in the player object
     */
    public void setDrawPile(Card[] drawPile) {
        this.getMe().setCurrentDrawPile(drawPile);
        this.informNewDrawPile();
    }

    /**
     * Getter for returning the current draw pile to make the draw pile available from the ConsoleView
     * Fassade pattern used to hide long getter calls of the model
     *
     * @return returns the current stored DrawPile
     */
    public Card[] getCurrentDrawPile() {
        return this.getMe().getCurrentDrawPile();
    }

    /**
     * Method used to store the information recieved through RestController for start Game
     * If all information is stored in model, the ConsoleView will be informed via event
     * In this "simple" implementation automatically the first bot and postion is choosen
     *
     * @param avaiableRobot    Availabe bots received through RestController
     * @param avaiablePosition Availabe positions received through RestController
     */
    public void startGame(List<String> avaiableRobot, List<Position> avaiablePosition) {
        this.me.setBot(new Robot(avaiableRobot.get(0)));
        this.me.setPosition(new Position(avaiablePosition.get(0).getX(), avaiablePosition.get(0).getY()));
        this.informStartGame();
    }


    /**
     * Method used to join a specific game identified by id
     *
     * @param gameId id of the game you want to join
     * @return returns true if the joining was successful
     */
    public boolean joinGame(String gameId) {
        if (this.httpController.joinGame(gameId, this.getMe())) {
            this.getMe().setCurrentgame(this.findGamebyID(gameId, gameList()));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Helper method to find a specified game in a given list of games
     * Known Problems: Chek if it is really necessary to give list of games as input parameter...
     *
     * @param gameId      Id of game you want to find
     * @param listOfGames List in which you want to search
     * @return Returns the requested information as a Game object
     */
    public Game findGamebyID(String gameId, List<Game> listOfGames) {
        return listOfGames.stream().filter(x -> x.getId().equals(gameId)).findAny().orElse(null);
    }

    /**
     * Used to send a request to the Game Master for a full list of available games
     *
     * @return list of games which are available
     */
    public List<Game> gameList() {
        return this.httpController.getGameListPost();
    }

    /**
     * GameStatusRequest method can be used to request further information about a game at the Game Master
     * Known Problem: never used... :(, Idea update the current stored games with the additional information
     *
     * @param id GameID for which you want more information
     * @return returns the information in a Game object format
     */
    public Game gameStatusRequest(String id) {
        return httpController.gameStatusFromMaster(id);
        //TODO add addtional values to the object in GameList, that every time the object gets refreshed with new values
    }

    /**
     * Fassade getter Method to hide model getter calls
     * Was rewritten to make it testable
     *
     * @return return the latest game state of the current game in a State object
     */
    public State getCurrentGameState() {
        return this.getMe().getCurrentgame().getCurrentStateOfPlayer(this.getMe().getBot());
    }


}

package roboclient.controller.http;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * First exception implementation for special case, if secret is not found
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class SecretNotFoundException extends RuntimeException {
    public SecretNotFoundException(String message) {
        super(message);
    }

}

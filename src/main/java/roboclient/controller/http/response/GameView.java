package roboclient.controller.http.response;

import java.util.List;

/**
 * Player node gets list of games available in master server with joined players list
 */
public class GameView {
    private String id;
    private String name;
    private int maxRobotCount;
    private int currentRobotCount;
    private String status;
    private List<String> playerNames;

    /**
     * Empty constructor. Needed when incoming requests body data needs to be deserialized into an instance of {@link GameView}
     */
    public GameView() {
    }

    public GameView(String id, String name, int maxRobotCount, int currentRobotCount, String status, List<String> playerNames) {
        this.id = id;
        this.name = name;
        this.maxRobotCount = maxRobotCount;
        this.currentRobotCount = currentRobotCount;
        this.status = status;
        this.playerNames = playerNames;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getMaxRobotCount() {
        return maxRobotCount;
    }

    public void setMaxRobotCount(final int maxRobotCount) {
        this.maxRobotCount = maxRobotCount;
    }

    public int getCurrentRobotCount() {
        return currentRobotCount;
    }

    public void setCurrentRobotCount(final int currentRobotCount) {
        this.currentRobotCount = currentRobotCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getPlayerNames() {
        return playerNames;
    }

    public void setPlayerNames(List<String> playerNames) {
        this.playerNames = playerNames;
    }
}

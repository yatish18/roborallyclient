package roboclient.controller.http.response;

/**
 * From  Master node to player Game Id and its details are received once game is created
 */
public class GameCreateResponse {
    private String id;
    private String name;
    private int maxRobotCount;
    private int currentRobotCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxRobotCount() {
        return maxRobotCount;
    }

    public void setMaxRobotCount(int maxRobotCount) {
        this.maxRobotCount = maxRobotCount;
    }

    public int getCurrentRobotCount() {
        return currentRobotCount;
    }

    public void setCurrentRobotCount(int currentRobotCount) {
        this.currentRobotCount = currentRobotCount;
    }

    public GameCreateResponse() {
        this.setId("0000");
        this.setCurrentRobotCount(0);
        this.setMaxRobotCount(0);
        this.setName("default");
    }

    public GameCreateResponse(String id, String name, int maxRobotCount, int currentRobotCount) {
        this();
        this.id = id;
        this.name = name;
        this.maxRobotCount = maxRobotCount;
        this.currentRobotCount = currentRobotCount;
    }
}

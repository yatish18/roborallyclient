package roboclient.controller.http.response;

import java.util.List;

/**
 * Game Status can be viewed when player joined game
 * Game Status is fully implemented and working but not shown to player
 */
public class GameStatusResponse {
    private String id;
    private String name;
    private int maxRobotCount;
    private int currentRobotCount;
    private String status;
    private List<String> playerNames;

    public GameStatusResponse() {
        this.id = "0000";
        this.name = "";
        this.maxRobotCount = 0;
        this.currentRobotCount = 0;
        this.status = "";
    }

    public GameStatusResponse(String id, String name, int maxRobotCount, int currentRobotCount, String status, List<String> playerNames) {
        this.id = id;
        this.name = name;
        this.maxRobotCount = maxRobotCount;
        this.currentRobotCount = currentRobotCount;
        this.status = status;
        this.playerNames = playerNames;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxRobotCount() {
        return maxRobotCount;
    }

    public void setMaxRobotCount(int maxRobotCount) {
        this.maxRobotCount = maxRobotCount;
    }

    public int getCurrentRobotCount() {
        return currentRobotCount;
    }

    public void setCurrentRobotCount(int currentRobotCount) {
        this.currentRobotCount = currentRobotCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getPlayerNames() {
        return playerNames;
    }

    public void setPlayerNames(List<String> playerNames) {
        this.playerNames = playerNames;
    }
}

package roboclient.controller.http;

import roboclient.model.Card;
import roboclient.model.Game;
import roboclient.model.Player;

import java.util.List;

/**
 * Interface of HTTPGameController used for decoupling
 * Different "TransferImplementations" can implement this Interface
 * which allways uses the same methods to interact with the Game Manager
 */
public interface OutGoingAPI {
    Game createGame(Game mygame);
    boolean joinGame(String id, Player myplayer) throws IllegalAccessException;
    List<Game> getGameListPost();
    Game gameStatusFromMaster(String id);
    boolean exitGameToMaster(String id);
    boolean sendRegistersToMaster(String id, Card[] selectedCards);
}

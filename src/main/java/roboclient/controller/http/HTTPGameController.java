package roboclient.controller.http;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import roboclient.controller.SecretHandler;
import roboclient.controller.ViewConverter;
import roboclient.controller.http.post.data.GameCreateData;
import roboclient.controller.http.post.data.GameJoinData;
import roboclient.controller.http.post.data.SelectedCardData;
import roboclient.controller.http.response.GameCreateResponse;
import roboclient.controller.http.response.GameStatusResponse;
import roboclient.controller.http.response.GameView;
import roboclient.model.Card;
import roboclient.model.Game;
import roboclient.model.Player;

import java.util.List;

/**
 * Class for handling everything which is related to HTTP
 * It implements an Interface for decoupeling it, against the GameManager
 * For sending data it uses the Spring resttemplate
 */
@Component
public class HTTPGameController implements OutGoingAPI {
    private RestTemplate restTemplate;
    private String serverURI;
    Logger logger = Logger.getLogger(HTTPGameController.class.getName());

    /**
     * Getter for ServerURI
     *
     * @return returns the ServerURI as a String
     */
    public String getServerURI() {
        return serverURI;
    }


    /**
     * Autowire constructor for Spring
     *
     * @param restTemplate RestTemplate autowired in by Spring
     */
    @Autowired
    public HTTPGameController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.serverURI = "http://localhost:8090";
    }

    /**
     * Create Game for sending request for create game to Game Master
     *
     * @param mygame Game containing the information of the game you want to create
     * @return Returns a new Game containing the same information if the creation was successful
     */
    @Override
    public Game createGame(Game mygame) {
        GameCreateData sendGame = ViewConverter.convertGametoGameCreatePost(mygame);
        ResponseEntity<GameCreateResponse> response = restTemplate.postForEntity(this.getServerURI() + "/games/create", new HttpEntity<>(sendGame), GameCreateResponse.class);
        return ViewConverter.convertGameCreateResponsetoGame(response.getBody());
    }

    /**
     * Sending request for joining a specific game to Game Master
     *
     * @param id       id of the game you want to join
     * @param myplayer player which want to join the game
     * @return return true if everything was successful
     */
    @Override
    public boolean joinGame(String id, Player myplayer) {
        GameJoinData sendJoinRequest = ViewConverter.convertPlayertoGameJoinPost(myplayer);
        try {
            ResponseEntity<HttpEntity> response = restTemplate.postForEntity(this.getServerURI() + "/games/" + id + "/join", new HttpEntity<>(sendJoinRequest), HttpEntity.class);
            SecretHandler.setSecretKeyValue(SecretHandler.extractSecret(response));
            return true;

        } catch (HttpClientErrorException ex) {
            logger.error("Join Game Failed " + ex);

        }
        return false;

    }

    /**
     * Requesting Game Master for a list of all available games
     *
     * @return returns the list of available games on the Game Master
     */
    @Override
    public List<Game> getGameListPost() {
        String format = String.format("%s/games/list", this.getServerURI());
        ParameterizedTypeReference<List<GameView>> listOfGameView = new ParameterizedTypeReference<List<GameView>>() {
        };
        ResponseEntity<List<GameView>> data = restTemplate.exchange(format, HttpMethod.GET, null, listOfGameView);
        return ViewConverter.convertGameListObjecttoGame(data.getBody());
    }

    /**
     * Request for more specific Data for a specific Game
     *
     * @param id id of the game, for more specific game details
     * @return new Game including the new details
     */
    @Override
    public Game gameStatusFromMaster(String id) {
        String format = String.format("%s/games/%s/status", this.getServerURI(), id);
        ResponseEntity<GameStatusResponse> response = restTemplate.getForEntity(format, GameStatusResponse.class);
        return ViewConverter.convertGameStatusResponsetoGame(response.getBody());
    }

    /**
     * Method to leave a game properly
     *
     * @param id id of the game you want to leave
     * @return returns true if the game leaving procedure was successful, otherwise false
     */
    @Override
    public boolean exitGameToMaster(String id) {
        String format = String.format("%s/games/%s/leave", this.getServerURI(), id);
        ResponseEntity<HttpHeaders> response = restTemplate.postForEntity(format, SecretHandler.addSecretWithoutBody(), HttpHeaders.class);
        if (response.getStatusCodeValue() == 204 && SecretHandler.validateSecret(response.getHeaders())) {
            return true;
        }
        logger.info("Exit Game Failed");
        return false;
    }

    /**
     * Sending choosen cards from drawpile to Game Master
     *
     * @param id            For which game you want to sent your cards
     * @param selectedCards choosen cards
     * @return true if the procedure was successful, otherwise false
     */
    @Override
    public boolean sendRegistersToMaster(String id, Card[] selectedCards) {
        String format = String.format("%s/games/%s/round/sendRegisters", this.getServerURI(), id);
        SelectedCardData[] sendCardData = ViewConverter.convertCardArrayToCardDataArray(selectedCards);
        try {
            ResponseEntity<HttpEntity> response = restTemplate.postForEntity(format, SecretHandler.addSecret(new HttpEntity<>(sendCardData)), HttpEntity.class);
            if (response.getStatusCodeValue() == 204 && SecretHandler.validateSecret(response.getHeaders())) {
                return true;
            }
        } catch (HttpClientErrorException ex) {
            logger.error("Sending Register Failed" + ex);

        }
        return false;
    }
}
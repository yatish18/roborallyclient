package roboclient.controller.http.post.data;

import roboclient.model.Card;

/**
 * Sending 5 cards as array to master node
 */

public class SendRegistersData {
    private Card[] registers;

    public SendRegistersData(Card[] registers) {
        this.registers = registers;
    }

    public Card[] getRegisters() {
        return registers;
    }

    public void setRegisters(Card[] registers) {
        this.registers = registers;
    }
}

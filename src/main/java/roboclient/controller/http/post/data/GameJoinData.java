package roboclient.controller.http.post.data;

/**
 * Sending player name and url from joining game
 */

public class GameJoinData {
    private String playerName;
    private String playerNodeRestBaseUrl;

    public GameJoinData() {
        this.playerName = "default";
        this.playerNodeRestBaseUrl = "";
    }

    public GameJoinData(String playerName, String playerNodeRestBaseUrl) {
        this();
        this.playerName = playerName;
        this.playerNodeRestBaseUrl = playerNodeRestBaseUrl;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerNodeRestBaseUrl() {
        return playerNodeRestBaseUrl;
    }

    public void setPlayerNodeRestBaseUrl(String playerNodeRestBaseUrl) {
        this.playerNodeRestBaseUrl = playerNodeRestBaseUrl;
    }


}

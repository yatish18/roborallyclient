package roboclient.controller.http.post.data;

/**
 * Sending player name and maximum robot count from player node to master node
 */
public class GameCreateData {
    private String name;
    private int maxRobotCount;

    public GameCreateData() {
    }

    public GameCreateData(String name, int maxRobotCount) {
        this.name = name;
        this.maxRobotCount = maxRobotCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxRobotCount() {
        return maxRobotCount;
    }

    public void setMaxRobotCount(int maxRobotCount) {
        this.maxRobotCount = maxRobotCount;
    }
}

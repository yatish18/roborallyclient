package roboclient.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import roboclient.controller.http.SecretNotFoundException;

/**
 * SecretHandler used for handling all secret related interaction with Game Master
 * it includes storing the secret, validating and adding secret to passed entities
 */
public class SecretHandler {
    private static String secret;
    private static final String SECRETKEY = "secret";
    private static final String NO_SECRET_KEY_FOUND = "No Secret Key Found";
    static Logger logger = Logger.getLogger(SecretHandler.class.getName());

    private SecretHandler() {
        SecretHandler.setSecretKeyValue(SECRETKEY);
    }

    public static String getSECRETKEY() {
        return SECRETKEY;
    }

    public static String getSecretKeyValue() {
        return secret;
    }

    public static void setSecretKeyValue(String secretkey) {
        SecretHandler.secret = secretkey;
    }

    /**
     * Method for extracting the secret header value of a passed in response entity
     *
     * @param myentity Entity from you want to extract the secret
     * @param <T>      Type of the entity
     * @return returns the secret in type String
     */
    public static <T> String extractSecret(ResponseEntity<T> myentity) {
        String secret = "error";
        if (myentity.getHeaders().get(SECRETKEY) != null) {
            if (!myentity.getHeaders().get(SECRETKEY).isEmpty()) {

                secret = myentity.getHeaders().get(SECRETKEY).get(0);
            }
        } else {
            logger.info(NO_SECRET_KEY_FOUND);
            throw new SecretNotFoundException(NO_SECRET_KEY_FOUND);
        }

        return secret;
    }

    /**
     * Method for adding the own secret to a passed in entity
     * The method will use automatically the provided secret stored in the static secret variable,
     * with the Key stored in the static secretkey variable
     *
     * @param postObject Entity you want to which you want the secret
     * @param <T>        Type of the entity
     * @return Return the same entity just with added, secret to it
     */
    public static <T> HttpEntity<T> addSecret(HttpEntity<T> postObject) {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set(SECRETKEY, secret);
        return new HttpEntity<>(postObject.getBody(), myheader);

    }

    /**
     * Sometimes it is needed to respond without a "whole" entity
     * for this use-cases this method can be used to just, create a empty
     * entity with the added secret
     *
     * @return Return a entity only with the added secret
     */
    public static HttpEntity<HttpHeaders> addSecretWithoutBody() {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set(SECRETKEY, secret);
        return new HttpEntity<>(myheader);
    }

    /**
     * Method for validating HttpHeaders from different entities to check
     * if the passed method from the Game Master is equals to the own secret.
     * This implementation of validating requires that Game Master and Player Node
     * uses the same secret to communicate
     *
     * @param validatingString Passed in HttpHeader
     * @return Return boolean true or false if the secret is equal or not
     */
    public static boolean validateSecret(HttpHeaders validatingString) {
        if (validatingString.containsKey(SECRETKEY)) {
            if (!validatingString.get(SECRETKEY).isEmpty()) {
                if (validatingString.get(SECRETKEY).get(0).equals(secret)) {
                    return true;
                } else {
                    logger.info("Wrong secret!");
                    throw new SecretNotFoundException("Wrong secret!");
                }
            }
        }
        logger.info(NO_SECRET_KEY_FOUND);
        throw new SecretNotFoundException(NO_SECRET_KEY_FOUND);
    }

    /**
     * Another method for adding secret to entity, this method is only used for responseEntities
     * The first idea was to combine the two addSecret method to one, but unfortunatly we weren't able
     * to make this possible because the constructors for ResponseEntity and HttpEntity are different
     *
     * @param postObject Entity you want to add the secret
     * @param <T>        Type of the entity
     * @return Return a ne entity with the same body but added secret to it
     */
    public static <T> ResponseEntity<T> addSecretResponse(ResponseEntity<T> postObject) {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set(SECRETKEY, secret);
        return new ResponseEntity<>(postObject.getBody(), myheader, postObject.getStatusCode());
    }

}

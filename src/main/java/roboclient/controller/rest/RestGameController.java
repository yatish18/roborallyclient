package roboclient.controller.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import roboclient.controller.SecretHandler;
import roboclient.controller.ViewConverter;
import roboclient.controller.rest.post.RoundActionsRequest;
import roboclient.controller.rest.post.RoundStartRequest;
import roboclient.controller.rest.post.StartGameRequest;
import roboclient.controller.rest.response.StartGameResponse;
import roboclient.manager.GameManager;

/**
 * RestController including all necessary POST/GET Methods
 * Autowires in Game Manger to push incoming information to controller
 * Known Problems: Maybe using an Fassade pattern for Game Manager would be better for this class
 * or a Interface in Game Manager to define the methods more precisely
 */
@RestController
public class RestGameController {
    @Autowired
    private GameManager gameManager;

    Logger logger = Logger.getLogger(RestGameController.class.getName());

    /**
     * Method for receiving information, if a game is started.
     * Initiated from Game Master to Player Node
     * Simple error handling included for testing if the request is equal to the currentGame
     * and check if the right secret is included.
     *
     * @param id      id of the started game
     * @param body    including all necessary data or the actual payload of the post
     * @param headers including metadata like secret
     * @return Returns necessary data like choosen bot and start position
     */
    @PostMapping("/games/{id}/start")
    public ResponseEntity<StartGameResponse> startGame(@PathVariable String id, @RequestBody StartGameRequest body, @RequestHeader HttpHeaders headers) {
        if (this.gameManager.getMe().getCurrentgame().getId().equals(id) && SecretHandler.validateSecret(headers)) {
            this.gameManager.startGame(body.getAvailableRobots(), ViewConverter.convertPositionDataListToPositionList(body.getAvailableStartingPositions()));
            StartGameResponse responseObj = ViewConverter.convertPlayertoStartGameResponse(this.gameManager.getMe());
            ResponseEntity<StartGameResponse> response = new ResponseEntity<>(responseObj, HttpStatus.OK);
            return SecretHandler.addSecretResponse(response);
        }
        logger.info("No Robot and position selected ");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Method for receiving information, if a round is started.
     * Initiated from Game Master to Player Node
     * Simple error handling included for testing if the request is equal to the currentGame
     * and check if the right secret is included.
     * return with HttpStatusOk(200) if everything is fine
     * and with HttpStatusNotFound(404), if something goes wrong
     *
     * @param id      id of the game where a new round was introduced/started
     * @param body    actual payload of the post request, including drawPile, position....
     * @param headers extract header to check metadata of the secret like secret
     * @return return a simple HttpStatus
     */
    @PostMapping("/games/{id}/round/start")
    public ResponseEntity<HttpStatus> roundStart(@PathVariable String id, @RequestBody RoundStartRequest body, @RequestHeader HttpHeaders headers) {
        if (this.gameManager.getMe().getCurrentgame().getId().equals(id) && SecretHandler.validateSecret(headers)) {
            this.gameManager.setDrawPile(ViewConverter.convertCardDataArraytoCardArray(body.getDrawPile()));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        logger.info("Draw Pile not received");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /**
     * Method which will be triggered from Game Master
     * Used for receiving information about the changes in a specific round
     * Return HttpStatusNoContent(204), if everything is fine
     * Return HttpStatusNotFound(404), if something goes wrong
     *
     * @param id     id of the game to which the actions refer
     * @param body   payload of the request, including multiple list of postions
     * @param header extract the metadata of the request to verify the request by secret
     * @return respond with a simple http status
     */
    @PostMapping("/games/{id}/round/actions")
    public ResponseEntity<HttpStatus> roundActions(@PathVariable String id, @RequestBody RoundActionsRequest body, @RequestHeader HttpHeaders header) {
        if (this.gameManager.getMe().getCurrentgame().getId().equals(id) && SecretHandler.validateSecret(header)) {
            this.gameManager.newRound(ViewConverter.convertRoundActionsRequesttoRound(body.getPositions()));
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /**
     * Unused code which wasn't included due to time
     * The idea was to create a restinterface which provide all information and methods through
     * rest-methods to plug in any possible interface, like a WebGUI
     */
    /*
    --------------------------------------------------------
    ----------->>>>Rest-API (Testing-Phase)
    --------------------------------------------------------
    */
    /*@PostMapping("/games/{id}/join")
    public String postJoinGameWithId(@RequestBody Player player, @PathVariable int id ) throws SecretNotFoundException {
        return gameManager.joinGame(id,player);
    }

    @GetMapping("/games/list")
    public List<Game> getAllGameList(){
        return gameManager.gameList();
    }

    @PostMapping("/games/create")
    public Game postCreateNewGame(@RequestBody Game mygame){
        return gameManager.createGame(mygame.getName(),mygame.getMaxRoboCount());
    }

    @PostMapping("/player/create/{name}")
    public void createPlayer(@PathVariable String name){
        this.gameManager.enterPlayerName(name);
    }

    /*
    @GetMapping("/games/{id}/status")
    public Game getGameStatusWithId(@PathVariable int id){
        return null;
    }
    @PostMapping("/games/{id}/start")
    public Game postGameStartWithId(@PathVariable int id){
        return null;
    }
    ........*/
}

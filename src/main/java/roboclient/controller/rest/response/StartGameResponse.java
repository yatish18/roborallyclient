package roboclient.controller.rest.response;

import roboclient.controller.rest.post.data.PositionData;

/**
 * bot and player position is sent to master node once game is started
 */
public class StartGameResponse {
    private String robot;
    private PositionData position;

    public StartGameResponse() {
    }

    public StartGameResponse(String robot, PositionData position) {
        this.robot = robot;
        this.position = position;
    }

    public StartGameResponse(String robot, int x, int y) {
        this.position = new PositionData(x, y);
        this.robot = robot;
    }

    public String getRobot() {
        return robot;
    }

    public void setRobot(String robot) {
        this.robot = robot;
    }

    public PositionData getPosition() {
        return position;
    }

    public void setPosition(PositionData position) {
        this.position = position;
    }


}

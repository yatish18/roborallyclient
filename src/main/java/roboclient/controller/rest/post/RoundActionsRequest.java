package roboclient.controller.rest.post;

import roboclient.controller.rest.post.data.ActionsData;
import roboclient.controller.rest.post.data.PositionsData;

import java.util.Arrays;
import java.util.stream.Stream;

//TODO should not be here (logic should be in ViewConverter or somewhere else...)
public class RoundActionsRequest {
    private PositionsData[] positions;
    private ActionsData[] actions;

    public RoundActionsRequest(PositionsData[] positions, ActionsData[] actions) {
        this.positions = positions;
        this.actions = actions;
    }

    public RoundActionsRequest() {
        this.positions = new PositionsData[]{};
        this.actions = new ActionsData[]{};
    }

    public PositionsData[] getPositions() {
        return positions;
    }

    public ActionsData[] getActions() {
        return actions;
    }

    public void setPosition0(PositionsData[] positions) {
        this.addInputtoPositions(positions);
    }

    public void setPosition1(PositionsData[] positions) {
        this.addInputtoPositions(positions);
    }

    public void setPosition2(PositionsData[] positions) {
        this.addInputtoPositions(positions);
    }

    public void setPosition3(PositionsData[] positions) {
        this.addInputtoPositions(positions);
    }

    public void setPosition4(PositionsData[] positions) {
        this.addInputtoPositions(positions);
    }

    public void setPosition5(PositionsData[] positions) {
        this.addInputtoPositions(positions);
    }

    public void setAction0(ActionsData[] actions) {
        this.addInputtoActions(actions);
    }

    public void setAction1(ActionsData[] actions) {
        this.addInputtoActions(actions);
    }

    public void setAction2(ActionsData[] actions) {
        this.addInputtoActions(actions);
    }

    public void setAction3(ActionsData[] actions) {
        this.addInputtoActions(actions);
    }

    public void setAction4(ActionsData[] actions) {
        this.addInputtoActions(actions);
    }

    public void setAction5(ActionsData[] actions) {
        this.addInputtoActions(actions);
    }

    public void addInputtoPositions(PositionsData[] positions) {
        this.positions = Stream.concat(Arrays.stream(this.positions), Arrays.stream(positions)).toArray(PositionsData[]::new);
    }

    public void addInputtoActions(ActionsData[] actions) {
        this.actions = Stream.concat(Arrays.stream(this.actions), Arrays.stream(actions)).toArray(ActionsData[]::new);
    }
}

package roboclient.controller.rest.post.data;

/**
 * Model for receiving Action from master node
 */
public class ActionsData {
    private String playerId;
    private String action;
    private String[] damageCards;

    public ActionsData(String playerId, String action, String[] damageCards) {
        this.playerId = playerId;
        this.action = action;
        this.damageCards = damageCards;
    }

    public ActionsData() {
        this.playerId = "0000";
        this.damageCards = new String[]{""};
        this.action = "";
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String[] getDamageCards() {
        return damageCards;
    }

    public void setDamageCards(String[] damageCards) {
        this.damageCards = damageCards;
    }


}

package roboclient.controller.rest.post.data;

/**
 * Represents position of player in game
 */
public class PositionData {
    private int x;
    private int y;

    public PositionData() {
    }

    public PositionData(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}

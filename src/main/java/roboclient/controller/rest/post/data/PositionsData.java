package roboclient.controller.rest.post.data;

/**
 * Model for receiving Player Position from master node
 */
public class PositionsData {
    private String robot;
    private String direction;
    private String playerName;
    private String playerId;
    private PositionData position;
    private String[] checkpoints;

    public PositionsData() {
    }

    public PositionsData(String robot, String direction, String playerName, String playerId, PositionData position, String[] checkpoints) {
        this.robot = robot;
        this.direction = direction;
        this.playerName = playerName;
        this.playerId = playerId;
        this.position = position;
        this.checkpoints = checkpoints;
    }

    public String getRobot() {
        return robot;
    }

    public void setRobot(String robot) {
        this.robot = robot;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public PositionData getPosition() {
        return position;
    }

    public void setPosition(PositionData position) {
        this.position = position;
    }

    public String[] getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(String[] checkpoints) {
        this.checkpoints = checkpoints;
    }
}

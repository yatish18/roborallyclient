package roboclient.controller.rest.post.data;

/**
 * Model for receiving Field from master node
 */
public class FieldData {
    private int x;
    private int y;
    private String field;

    public FieldData() {
    }

    public FieldData(int x, int y, String field) {
        this.x = x;
        this.y = y;
        this.field = field;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}

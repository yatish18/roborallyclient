package roboclient.controller.rest.post.data;

import java.util.List;

/**
 * Model for receiving board from master node
 */
public class BoardData {
    private SizeData size;
    private List<FieldData> fields;

    public BoardData() {
    }

    public BoardData(SizeData size, List<FieldData> fields) {
        this.size = size;
        this.fields = fields;
    }

    public SizeData getSize() {
        return size;
    }

    public void setSize(SizeData size) {
        this.size = size;
    }

    public List<FieldData> getFields() {
        return fields;
    }

    public void setFields(List<FieldData> fields) {
        this.fields = fields;
    }


}

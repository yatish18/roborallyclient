package roboclient.controller.rest.post.data;

/**
 * Model for Board width and height
 */
public class SizeData {
    private int width;
    private int height;

    public SizeData() {
    }

    public SizeData(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


}

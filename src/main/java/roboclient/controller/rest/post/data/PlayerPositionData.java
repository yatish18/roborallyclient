package roboclient.controller.rest.post.data;

/**
 * Model for receiving Player position from master node
 */
public class PlayerPositionData {
    private String robot;
    private String directon;
    private String playerName;
    private String playerId;
    private PositionData position;

    public String getRobot() {
        return robot;
    }

    public void setRobot(String robot) {
        this.robot = robot;
    }

    public String getDirecton() {
        return directon;
    }

    public void setDirecton(String directon) {
        this.directon = directon;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public PositionData getPosition() {
        return position;
    }

    public void setPosition(PositionData position) {
        this.position = position;
    }

}

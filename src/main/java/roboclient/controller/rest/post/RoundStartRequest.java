package roboclient.controller.rest.post;

import roboclient.controller.rest.post.data.CardData;
import roboclient.controller.rest.post.data.PlayerPositionData;

import java.util.List;

/**
 * Model for receiving cards from master node
 */
public class RoundStartRequest {
    private List<PlayerPositionData> playerPositions;
    private CardData[] drawPile;

    public List<PlayerPositionData> getPlayerPositions() {
        return playerPositions;
    }

    public void setPlayerPositions(List<PlayerPositionData> playerPositions) {
        this.playerPositions = playerPositions;
    }

    public CardData[] getDrawPile() {
        return drawPile;
    }

    public void setDrawPile(CardData[] drawPile) {
        this.drawPile = drawPile;
    }


}

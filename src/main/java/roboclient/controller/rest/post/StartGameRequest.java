package roboclient.controller.rest.post;

import roboclient.controller.rest.post.data.BoardData;
import roboclient.controller.rest.post.data.PositionData;

import java.util.List;

/**
 * Model for receiving board details,available bots and its positions
 */
public class StartGameRequest {
    private BoardData board;
    private List<String> availableRobots;
    private List<PositionData> availableStartingPositions;

    public StartGameRequest() {
    }

    public StartGameRequest(BoardData board, List<String> availableRobots, List<PositionData> availableStartingPositions) {
        this.board = board;
        this.availableRobots = availableRobots;
        this.availableStartingPositions = availableStartingPositions;
    }

    public BoardData getBoard() {
        return board;
    }

    public void setBoard(BoardData board) {
        this.board = board;
    }

    public List<String> getAvailableRobots() {
        return availableRobots;
    }

    public void setAvailableRobots(List<String> availableRobots) {
        this.availableRobots = availableRobots;
    }

    public List<PositionData> getAvailableStartingPositions() {
        return availableStartingPositions;
    }

    public void setAvailableStartingPositions(List<PositionData> availableStartingPositions) {
        this.availableStartingPositions = availableStartingPositions;
    }

}

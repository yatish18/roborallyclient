package roboclient.controller.console;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import roboclient.manager.GameManager;
import roboclient.manager.event.RestEventDrawPile;
import roboclient.manager.event.RestEventNewState;
import roboclient.manager.event.RestEventStartGame;
import roboclient.model.Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * GUI-Class based on Commandline
 * One of the first implemented classes.
 * Started to include missing return types, optionals and streams after discussion in class (still in progress...)
 * Missing parts are due to consistency or time reasons are annotated with TODO
 */

@Component
public class ConsoleView implements CommandLineRunner {
    private boolean waitingLoop = true;
    private GameManager gameManager;
    private InputStreamReader option;
    private BufferedReader br;
    Logger logger = Logger.getLogger(ConsoleView.class.getName());

    /**
     * ConsoleView takes per DP GameManager Object to store Uservalues
     *
     * @param gmanager object injected via Spring (DPI)
     */
    @Autowired
    public ConsoleView(GameManager gmanager) {
        this.gameManager = gmanager;
        this.option = new InputStreamReader(System.in);
        this.br = new BufferedReader(option);
    }

    /**
     * EventListener for specific RestEventStartGame event.
     * Triggered, if Game is started.
     * Known Problems: Change name of Event Object; Don't allow event to be triggered anytime
     *
     * @param event listens on specific RestEventStartGame event
     */
    @EventListener
    public void onApplicationEvent(RestEventStartGame event) {
        this.consolePrinter(this.printGameStart());
    }

    /**
     * EventListener for specific RestEventDrawPile event
     * Triggered, if new drawPile is sent
     * Known Problems: Change name of Event Object; Don't allow event to be triggered anytime; Maybe remove looping logic from EventListener; Like in the whole application include proper Exception handling.
     *
     * @param event listens on specific RestEventDrawPile event
     * @throws IOException forwarded exception from Bufferreader
     */
    @EventListener
    public void onApplicationEvent(RestEventDrawPile event) throws IOException {
        while (!this.chooseFromDrawPile()) {
            //Waiting for player to choose pile, If something goes wrong it is reprinted on screen
        }
    }

    /**
     * EventListener for specific RestEventNewState
     * Triggered, if a Round is completed and a new currentState is available
     * Known Problems: Change name of Event Object; Don't allow to be triggered anytime
     *
     * @param event listens on specific RestEventNewState event
     */
    @EventListener
    public void onApplicationEvent(RestEventNewState event) {
        this.consolePrinter(this.printCurrentState());
    }

    /**
     * Overritten method for CommandLineRunner to define "starting" method
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        this.initializeView();
    }

    /**
     * Initial "starting" method
     * Known Problems: Messy method should be rewritten
     */
    public void initializeView() {
        boolean exitGame = true;

        System.out.println("**********************Welcome To Roborally Game***************************");
        while (exitGame) {
            this.consolePrinter(printMenu());
            try {
                String choice = this.br.readLine();
                this.waitingLoop = true;
                System.out.println("-----------------------------------------------");
                switch (Integer.parseInt(choice)) {
                    case 1: //Create New Game
                        createGame();
                        break;
                    case 2: //List all Games
                        this.consolePrinter(printAvailableGames());
                        break;
                    case 3: //Join Game
                        joinGame();
                        waitingForNextExent();
                        break;
                    case 4: //Exit Game
                        System.out.println("Good Bye!!");
                        exitGame = false;
                        break;
                    default:
                        return;
                }

            } catch (Exception ex) {
                logger.error("Error", ex);
            }
        }
    }

    /**
     * Builds a String for showing that the currentGame is started
     * including: Bot and Startposition
     *
     * @return builded String
     */
    public String printGameStart() {
        return "Game started... \n" +
                "-----------------------------------------------\n" +
                "Bot: " + this.gameManager.getMe().getBot().getName() + "\n" +
                "Start Position \n" +
                "X: " + this.gameManager.getMe().getStartPosition().getX() + "\n" +
                "Y: " + this.gameManager.getMe().getStartPosition().getY() + "\n" +
                "-----------------------------------------------\n";
    }

    /**
     * General printer-method to avoid using sout all the time
     * some methods are already switched to this
     *
     * @param input String input you want to print
     * @return true if printing was successful
     */
    public boolean consolePrinter(String input) {
        System.out.println(input);
        return true;
    }

    /**
     * Builds a String for showing the currentstate of the Game
     * including: Position and reached Checkpoints
     * Known Problems: Internal-Method-Calls include a lot of recomputation and should be more decoupled
     *
     * @return returns produced String of this method
     */
    public String printCurrentState() {
        StringBuilder output = new StringBuilder();
        output.append("Round successfully completed!\n");
        output.append("Your current position is the following:\n");
        output.append("-----------------------------------------------\n");
        output.append("Position X: " + this.gameManager.getCurrentGameState().getPosition().getX() + "\n");
        output.append("Position Y: " + this.gameManager.getCurrentGameState().getPosition().getY() + "\n");
        output.append("-----------------------------------------------\n");
        if (!(this.gameManager.getCurrentGameState().getCheckpoints().isEmpty())) {
            output.append("You already reached the following Checkpoints: \n");
            this.gameManager.getCurrentGameState().getCheckpoints().stream().forEach(element -> output.append(element + " "));
        }
        return output.toString();
    }

    /**
     * Builds a String for printing out the currentDrawPile
     * Known Problems: Proper Exception handling...
     *
     * @return produced String
     */
    public String printDrawPile() {
        StringBuilder output = new StringBuilder();
        output.append("\nCurrent Draw Pile:\n");
        output.append("-----------------------------------------------\n");
        for (int i = 0; i < this.gameManager.getCurrentDrawPile().length; i++) {
            output.append("[" + i + "] " + this.gameManager.getCurrentDrawPile()[i].name() + "\n");
        }
        output.append("-----------------------------------------------\n");
        return output.toString();
    }

    /**
     * Build a String for printing out user-instructions for DrawPile
     *
     * @return produced String
     */
    public String printDrawPileHeader() {
        return "Please Input Index of Cards in movement order:\n" +
                "-----------------------------------------------\n" +
                "Please enter exactly 5 Cards!\n" +
                "Each Card can only used once!\n" +
                "Seperate Card Indexes by using commas[,]\n" +
                "-----------------------------------------------\n";
        }

    /**
     * Coordination and choosing method for DrawPile
     * Known Problems: Proper Exception handling, maybe split up in multipile methods, you can only exit the game if you type here quit... :(,
     * returns also true if the user want to exit game (dirty fix... needs to be solved properly)
     * @return returns true if choosen drawpile is final, otherwise false
     * @throws IOException (Bufferreader)
     */
    public boolean chooseFromDrawPile() throws IOException {
        if (!(this.consolePrinter(this.printDrawPileHeader()) && this.consolePrinter(this.printDrawPile()))) {
            logger.info("Something went wrong while printing the Pile..."); //TODO change to info.error?
            return false;
        }
        String dropPileInput = this.br.readLine();
        if (exitGame(dropPileInput)) {
            if (this.dropPileValidator(dropPileInput).isPresent()) {
                int[] indexes = this.dropPileValidator(dropPileInput).orElse(new int[]{});
                if (this.displaySelectedPile(indexes)) {
                    if (this.gameManager.mapAndSendRegisters(indexes)) {
                        logger.info("Registeres are sent to GameMaster");
                        return true;
                    }
                }
            }
        }else {
            return true; //Needs to be changed
        }
        return false;
    }

    /**
     * Recently rewirtten method for validating DrawPile userinput
     * Return an Optional and used Streamprocessing for checking the data
     * Known-Problems: Name???
     *
     * @param pileChoice user/console input, of choosen pile in indexes
     * @return returns as an optional an array of int, if all validation was successful
     */
    public Optional<int[]> dropPileValidator(String pileChoice) {
        String[] indexInput;
        int[] indexReturn = new int[5];
        String pileChoiceAfterReplace = pileChoice.replaceAll("\\s+", "");
        indexInput = pileChoiceAfterReplace.split(",");
        if (indexInput.length != 5) {
            return Optional.empty();
        }
        //Convert String input into Int-Indexes
        List<Integer> returnList = Arrays.stream(indexInput).map(x -> Integer.parseInt(x)).collect(Collectors.toList());
        for (int i = 0; i < returnList.size(); i++) {
            indexReturn[i] = returnList.get(i);
        }
        //Check if userchoice is exactly five
        if (Arrays.stream(indexReturn).filter(x -> x < this.gameManager.getMe().getCurrentDrawPile().length).count() != 5) {
            return Optional.empty();
        }
        //Check if there are duplicates in userchoice
        if (Arrays.stream(indexInput).distinct().count() != 5) {
            return Optional.empty();
        }
        //TODO Check if Cards are used twice in Input

        return Optional.of(indexReturn);
    }


    /**
     * Method for displaying choosen Cards from DrawPile for verification
     * Known Problems: No proper exception handling..., using consolePrinter?, duplicated code?
     *
     * @param selectedPile push in indexes of cards as an int array
     * @return returns true if choosen drawPile indexes are final
     * @throws IOException (Bufferreader)
     */
    private boolean displaySelectedPile(int[] selectedPile) throws IOException {
        System.out.println("---------------------------------");
        System.out.println("Please Verify your selected pile");
        System.out.println("---------------------------------");
        Arrays.stream(selectedPile).forEach(i -> System.out.println(this.gameManager.getMe().getCurrentDrawPile()[i].name()));
        System.out.println("---------------------------------");
        System.out.println("1.Confirm Selected cards");
        System.out.println("2.Re-Select cards");
        String choice = this.br.readLine();
        if ("1".equals(choice)) {
            return true;
        } else if ("2".equals(choice)) {
            return false;
        }
        System.out.println("Wrong Value");
        System.out.println("");
        return false;
    }

    /**
     * Method for creating a new Player
     * Known Problems: Outsourcing to consolePrinter, no proper Exception handling...
     *
     * @throws IOException (Bufferreader)
     */
    public void enterYourName() throws IOException {
        System.out.println("\n-----------------------------------------------");
        System.out.println("Please Enter your Name:");
        String playerName = br.readLine();
        this.gameManager.enterPlayerName(playerName);
        System.out.println("Welcome " + playerName + " to the Roborally Game");
        System.out.println("-----------------------------------------------");
    }

    /**
     * Method for checking if userinput includes specific quit sequence
     *
     * @param exitvalue userinput vale
     * @return returns true if userinput is not including an exit game statement
     */
    public boolean exitGame(String exitvalue) {
        if ("Q".equalsIgnoreCase(exitvalue) || "QUIT".equalsIgnoreCase(exitvalue)) {
            boolean status = gameManager.exitGame();
            if (status) {
                System.out.println("You have sucessfully Exited Game, Thanks For Playing");
                System.out.println("");
            } else {
                System.out.println("Failed to Exit Game");
                return true;
            }
            this.waitingLoop = false;
            return false;
        } else {
            return true;
        }
    }

    /**
     * Builds a string for displaying the User-Menu
     *
     * @return produced String
     */
    public String printMenu() {
        return "Please select any option below\n" +
                "1.Create New Game\n" +
                "2.List of Games\n" +
                "3.Join Game\n" +
                "4.Exit Game\n" +
                "-----------------------------------------------\n";
    }

    /**
     * Builds a string for displaying the current available games
     * Known Problems: gameManager.gamelist -> gameManager.getGamelist
     *
     * @return produced String
     */
    public String printAvailableGames() {
        StringBuilder output = new StringBuilder();
        output.append("\nAvailable Games\n");
        output.append("-----------------------------------------------\n");
        List<Game> list = this.gameManager.gameList();
        for (Game game : list) {
            output.append("ID:" + game.getId() + "\n");
            output.append("Game Name:" + game.getName() + "\n");
            output.append("Current RoboCount:" + game.getCurrentRoboCount() + "\n");
            output.append("Max RoboCount:" + game.getMaxRoboCount() + "\n");
            output.append("-----------------------------------------------\n");
        }
        return output.toString();
    }

    /**
     * Old looping method for waiting for events
     */
    public void waitingForNextExent() {
        System.out.println("Waiting for Game Master");
        while (this.waitingLoop) {
        }
    }

    /**
     * Method for creating a new Game
     * Known Problems: Naming duplicate with GameManager..., No proper Exception handling...
     *
     * @throws IOException (Bufferreader)
     */
    public void createGame() throws IOException {
        System.out.println("Pease Enter your Gamename");
        String gamename = br.readLine();
        System.out.println("Please Enter Maximum Robot count");
        String robotcount = br.readLine();
        this.gameManager.createGame(gamename, Integer.parseInt(robotcount));
        System.out.println("Game: " + this.gameManager.getCurrentGameName() + " sucessfully created!");
        System.out.println("-----------------------------------------------");
    }

    /**
     * Method for handling joining interactions
     * Known Problems: Naming duplicate with GameManager..., No proper Exception handling...
     *
     * @throws IOException (Bufferreader)
     */
    public void joinGame() throws IOException {
        this.enterYourName();
        this.consolePrinter(this.printAvailableGames());
        System.out.println("\nPlease enter GameID of the Game you want to join:");
        System.out.println("-----------------------------------------------");
        String gameId = br.readLine();
        if (this.gameManager.joinGame(gameId)) {
            System.out.println("You have sucessfully joined the Game: " + this.gameManager.getMe().getCurrentgame().getName());
        } else {
            System.out.println("Something went wrong...please try again");
            this.joinGame();
            return;
        }
        System.out.println("-----------------------------------------------");
    }
}


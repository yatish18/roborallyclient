package roboclient.controller;

import roboclient.controller.http.post.data.GameCreateData;
import roboclient.controller.http.post.data.GameJoinData;
import roboclient.controller.http.post.data.SelectedCardData;
import roboclient.controller.http.response.GameCreateResponse;
import roboclient.controller.http.response.GameStatusResponse;
import roboclient.controller.http.response.GameView;
import roboclient.controller.rest.post.data.CardData;
import roboclient.controller.rest.post.data.PositionData;
import roboclient.controller.rest.post.data.PositionsData;
import roboclient.controller.rest.response.StartGameResponse;
import roboclient.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class with only static methods
 * it is used to convert all incoming/outgoing temporary HTTP/Rest objects
 * into objects which are internally used in the application (model objects)
 */
public class ViewConverter {

    /**
     * Method to convert an CardData Array into a Card Array, both classes are almost the same
     * but splittet in the application to decouple incoming data from data used in the model
     *
     * @param recievedDrawPile incoming CardData data
     * @return returns in model used cards
     */
    public static Card[] convertCardDataArraytoCardArray(CardData[] recievedDrawPile) {
        return Arrays.stream(recievedDrawPile).map(s -> Card.valueOf(s.name())).collect(Collectors.toList()).toArray(new Card[recievedDrawPile.length]);
    }

    /**
     * Method to convert a CardArray (Model) into a CardDataArray used for outgoing sending Cards
     * Known Problems: SelectedCardData should be CardData
     *
     * @param cards "Normal" Cards you want to convert to CardData
     * @return CardData with the same values as the input
     */
    public static SelectedCardData[] convertCardArrayToCardDataArray(Card[] cards) {
        return Arrays.stream(cards).map(s -> SelectedCardData.valueOf(s.name())).collect(Collectors.toList()).toArray(new SelectedCardData[cards.length]);
    }

    /**
     * Method to convert a Game object into a GameCreatePost object
     * Known Problems: Change Classname GameCreateData to GameCreatePost
     *
     * @param mygame object you want to convert
     * @return new GameCreatePost object with the extracted necessary values
     */
    public static GameCreateData convertGametoGameCreatePost(Game mygame) {
        return new GameCreateData(mygame.getName(), mygame.getMaxRoboCount());
    }

    /**
     * Method to convert a Player object into a StartGameResponse object
     *
     * @param myplayer player object which is used as input for the StartGameResponse object
     * @return new StartGameResponse object with the included data gathered from the player object
     */
    public static StartGameResponse convertPlayertoStartGameResponse(Player myplayer) {
        return new StartGameResponse(myplayer.getBot().getName(), myplayer.getStartPosition().getX(), myplayer.getStartPosition().getY());
    }

    /**
     * Method to convert a GameCreateResponse into a Game object used in the model
     *
     * @param myresponse response from the Game Master from which you want to extract, the information
     * @return new Game object which includes this information
     */
    public static Game convertGameCreateResponsetoGame(GameCreateResponse myresponse) {
        return new Game(myresponse.getName(), myresponse.getMaxRobotCount(), myresponse.getCurrentRobotCount(), myresponse.getId());
    }

    /**
     * Method to convert a GameStatusResponse into a Game object
     *
     * @param response GameStatusResponse object from which you want to extract information
     * @return new Game object which includes this information
     */
    public static Game convertGameStatusResponsetoGame(GameStatusResponse response) {
        return new Game(response.getName(), response.getMaxRobotCount(), response.getCurrentRobotCount(), response.getId(), response.getStatus(), response.getPlayerNames());
    }

    /**
     * Method to convert a Player object into a GameJoinPost object
     * Known Problems: Change name from GameJoinData to GameJoinPost
     *
     * @param myplayer player object which includes the required data
     * @return new GameJoinObject which was created out of the information stored in the Player object
     */
    public static GameJoinData convertPlayertoGameJoinPost(Player myplayer) {
        return new GameJoinData(myplayer.getName(), myplayer.getClientURI());
    }

    /**
     * Method to convert list of GameViews (Temporary Http Objects) into in the model stores list of Games
     * Known Problems: Name inconsistency (GameListObject,List<GameView>)
     *
     * @param list data which was received through HTTP (Resttemplate)
     * @return List of Games (Used in Model to store Game relevant data)
     */
    public static List<Game> convertGameListObjecttoGame(List<GameView> list) {
        return list.stream().map(x -> new Game(x.getName(), x.getMaxRobotCount(), x.getCurrentRobotCount(), x.getId())).collect(Collectors.toList());
    }

    /**
     * Method to convert PositionData to Position
     *
     * @param availableStartingPositions PostionData stored in a list
     * @return the same data but as a list of Position
     */
    public static List<Position> convertPositionDataListToPositionList(List<PositionData> availableStartingPositions) {
        List<Position> data = new ArrayList<>();
        for (PositionData eachPositiondata : availableStartingPositions) {
            data.add(new Position(eachPositiondata.getX(), eachPositiondata.getY()));
        }
        return data;
    }

    /**
     * Method to convert the RoundAction data into a in the model stored Round object
     * due to the complexity of this conversion it is splitted up in multiple
     * methods.
     * Known Problems: Misleading Classnames(PositionsData, PositionData)
     *
     * @param positionsData incoming request data
     * @return all needed data included in a Round object
     */
    public static Round convertRoundActionsRequesttoRound(PositionsData[] positionsData) {
        return new Round(ViewConverter.convertPositionsDatatoStateList(positionsData));
    }

    /**
     * See above
     * Converts PositionsData to a list of States
     *
     * @param positionsData PositionData Array
     * @return State list
     */
    private static List<State> convertPositionsDatatoStateList(PositionsData[] positionsData) {
        return Arrays.stream(positionsData).map(element -> new State(element.getPlayerId(), ViewConverter.convertStringArraytoCheckpointList(element.getCheckpoints()), ViewConverter.convertPostionDatatoPostion(element.getPosition()), ViewConverter.convertingFromStringRobotToRobotObject(element.getRobot()))).collect(Collectors.toList());
    }

    private static Robot convertingFromStringRobotToRobotObject(String bot) {
        return new Robot(bot);
    }

    /**
     * See above
     * Converts a String list of Checkpoints into a Enum list of Checkpoints
     *
     * @param checkpoints Checkpoint Array as String
     * @return Checkpoint list as Enum
     */
    private static List<Checkpoint> convertStringArraytoCheckpointList(String[] checkpoints) {
        return Arrays.stream(checkpoints).map(element -> Checkpoint.valueOf(element)).collect(Collectors.toList());
    }

    /**
     * See above
     * Converts a PositionData object into a Position
     *
     * @param position PositionData (Http/Rest)
     * @return Position(Model)
     */
    private static Position convertPostionDatatoPostion(PositionData position) {
        return new Position(position.getX(), position.getY());
    }
}

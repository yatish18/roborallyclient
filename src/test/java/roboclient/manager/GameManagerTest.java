package roboclient.manager;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import roboclient.controller.http.HTTPGameController;
import roboclient.model.Card;
import roboclient.model.Game;
import roboclient.model.Position;
import roboclient.model.Round;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;


@RunWith(MockitoJUnitRunner.class)
public class GameManagerTest {

    @Mock
    private HTTPGameController httpGameController;

    @Mock
    private ApplicationEventPublisher publisher;
    @InjectMocks
    private GameManager gameManager;


    @Test
    public void createGame() throws Exception {
        Mockito.when(httpGameController.createGame(anyObject())).thenReturn(new Game("test", 5, 3, "123"));
        gameManager.createGame("test", 5);
        Assert.assertEquals(gameManager.getMe().getCurrentgame().getMaxRoboCount(), 5);
    }

    @Test
    public void joinGameWhenSuccess() throws Exception {

        Mockito.when(httpGameController.joinGame(anyString(), anyObject())).thenReturn(true);

        boolean status = gameManager.joinGame("123");
        Assert.assertEquals(status, true);
        //  Assert.assertEquals(gameManager.getMe().getCurrentgame().getId(),123);
    }

    @Test
    public void joinGameWhenFailed() throws Exception {
        Mockito.when(httpGameController.joinGame(anyString(), anyObject())).thenReturn(false);
        boolean status = gameManager.joinGame("123");
        Assert.assertEquals(status, false);
    }

    @Test
    public void findGameById() throws Exception {
        List<Game> listOfGame = new ArrayList<>();
        listOfGame.add(new Game("test1", 5, 3, "123"));
        listOfGame.add(new Game("test2", 6, 2, "124"));
        Game data = gameManager.findGamebyID("123", listOfGame);
        Assert.assertEquals(data.getId(), "123");
    }

    @Test
    public void mapAndSendRegisters() throws Exception {
        int[] index = {1, 2, 3, 4, 5};
        Mockito.when(httpGameController.sendRegistersToMaster(anyString(), any(Card[].class))).thenReturn(true);
        boolean status = gameManager.mapAndSendRegisters(index);
        Assert.assertEquals(true, status);
        Assert.assertEquals(5, gameManager.getMe().getSelectedDrawPile().length);
    }

    @Test
    public void startGame() throws Exception {
        List<String> availableBot = new ArrayList<>();
        availableBot.add("hulk");
        availableBot.add("Hammer Bot");
        List<Position> positionData = new ArrayList<>();
        positionData.add(new Position(3, 3));
        positionData.add(new Position(5, 8));
        doNothing().when(publisher).publishEvent(any());
        gameManager.startGame(availableBot, positionData);
        Assert.assertEquals("hulk", gameManager.getMe().getBot().getName());
    }

    @Test
    public void gameList() throws Exception {
        List<Game> listOfGame = new ArrayList<>();
        listOfGame.add(new Game("test1", 5, 3, "123"));
        listOfGame.add(new Game("test2", 6, 2, "124"));
        Mockito.when(httpGameController.getGameListPost()).thenReturn(listOfGame);
        List<Game> data = gameManager.gameList();
        Assert.assertEquals(data.size(), 2);
        Assert.assertEquals(data.stream().filter(x -> x.getId().equals("123")).map(x -> x.getId()).count(), 1);
    }

    @Test
    public void gameStatusRequest() throws Exception {
        Mockito.when(httpGameController.gameStatusFromMaster(anyString())).thenReturn(new Game("test1", 5, 3, "123", "Not-Satrted", Arrays.asList("Hello", "World!")));
        Game data = gameManager.gameStatusRequest(anyString());
        Assert.assertEquals("Hello", data.getPlayers().get(0));
    }

    @Test
    public void setDrawPile() throws Exception {
        Card[] cards = {Card.MOVE1, Card.MOVE2, Card.AGAIN, Card.LEFT_TURN, Card.MOVE_BACK, Card.LEFT_TURN, Card.RIGHT_TURN, Card.UTURN, Card.MOVE_BACK};
        doNothing().when(publisher).publishEvent(any());
        gameManager.setDrawPile(cards);
        Assert.assertEquals(Card.MOVE1, gameManager.getMe().getCurrentDrawPile()[0]);
    }

    @Test
    public void exitGame() throws Exception {
        Mockito.when(httpGameController.exitGameToMaster(anyString())).thenReturn(true);
        boolean status = gameManager.exitGame();
        Assert.assertEquals(true, status);
    }

    @Test
    public void newRound() throws Exception {
        Round testinputdata = new Round();
        this.gameManager.newRound(testinputdata);
        Round testoutputdata = this.gameManager.getMe().getCurrentgame().getRounds().get(this.gameManager.getMe().getCurrentgame().getRounds().size() - 1);
        Assert.assertThat(testinputdata, equalTo(testoutputdata));
    }
}
package roboclient.controller.http;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import roboclient.controller.SecretHandler;
import roboclient.model.Card;
import roboclient.model.Game;
import roboclient.model.Player;

import java.util.List;

import static org.springframework.test.web.client.ExpectedCount.manyTimes;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@WebMvcTest(controllers = {HTTPGameController.class})
@RunWith(SpringRunner.class)
public class HTTPGameControllerTest {

    @Autowired
    HTTPGameController httpGameController;

    @Autowired
    RestTemplate restTemplate;

    MockRestServiceServer server;

    private String url;

    @Before
    public void setUp() {
        this.server = MockRestServiceServer.createServer(restTemplate);
        SecretHandler.setSecretKeyValue("secret");
        this.url = "http://localhost:8090/";
    }

    @Test
    public void createGame() throws Exception {
        server.expect(manyTimes(), requestTo(url + "games/create")).andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess("\n" +
                        "{      \n" +
                        "    \"id\": \"ZDUADAJ789\",\n" +
                        "    \"name\": \"Mikes game\",\n" +
                        "    \"maxRobotCount\": 6,\n" +
                        "    \"currentRobotCount\": 0\n" +
                        "}", MediaType.APPLICATION_JSON));

        //restTemplate.getForObject("/games/create", GameCreateResponse.class);
        Game object = httpGameController.createGame(new Game());
        server.verify();
        Assert.assertEquals("ZDUADAJ789", object.getId());
    }

    @Test
    public void joinGameSuccessfuly() throws Exception {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set("secret", "secret");
        server.expect(manyTimes(), requestTo(url + "games/123/join")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(200)).headers(myheader));
        Boolean status = httpGameController.joinGame("123", new Player());
        server.verify();
        Assert.assertEquals(true, status);
    }


    @Test
    public void joinGameNotSuccessful() throws Exception {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set("secret", "secret");
        server.expect(manyTimes(), requestTo(url + "games/123/join")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(404)).headers(myheader));
        Boolean status = httpGameController.joinGame("123", new Player());
        server.verify();
        Assert.assertEquals(false, status);
    }

    @Test
    public void getGameList() throws Exception {
        server.expect(manyTimes(), requestTo(url + "games/list")).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("[{\n" +
                        "\t\"id\": \"ZDAD5216\",\n" +
                        "\t\"name\": \"Jan-Philipps game\",\n" +
                        "\t\"maxRobotCount\": 6,\n" +
                        "\t\"currentRobotCount\": 5,\n" +
                        "\t\"status\": \"STARTED\",\n" +
                        "\t\"playerNames\": [\"Max\", \"Peter\", \"Michael\", \"Susi\", \"Steven\"]\n" +
                        "}]", MediaType.APPLICATION_JSON));
        List<Game> list = httpGameController.getGameListPost();
        server.verify();
        Assert.assertEquals("ZDAD5216", list.get(0).getId());
    }

    @Test
    public void gameStatusFromMaster() throws Exception {
        server.expect(manyTimes(), requestTo(url + "games/ZDAD5216/status")).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("\n" +
                        "{\n" +
                        "        \"id\": \"ZDAD5216\",\n" +
                        "        \"name\": \"Jan-Philipps game\",\n" +
                        "        \"maxRobotCount\": 2,\n" +
                        "        \"currentRobotCount\": 1,\n" +
                        "        \"status\": \"NOT_STARTED\",\n" +
                        "        \"playerNames\": [\"Max\"]\n" +
                        "}", MediaType.APPLICATION_JSON));
        Game game = httpGameController.gameStatusFromMaster("ZDAD5216");
        server.verify();
        Assert.assertEquals("NOT_STARTED", game.getStatus());
    }

    @Test
    public void exitGameToMasterSuccessful() throws Exception {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set("secret", "secret");
        server.expect(manyTimes(), requestTo(url + "games/123/leave")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(204)).headers(myheader));
        Boolean status = httpGameController.exitGameToMaster("123");
        server.verify();
        Assert.assertEquals(true, status);
    }

    @Test
    public void exitGameToMasterFailedWithWrongStatus() throws Exception {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set("secret", "secret");
        server.expect(manyTimes(), requestTo(url + "games/123/leave")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(200)).headers(myheader));
        Boolean status = httpGameController.exitGameToMaster("123");
        server.verify();
        Assert.assertEquals(false, status);
    }

    @Test
    public void sendRegistersToMaster() throws Exception {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set("secret", "secret");
        Card[] cards = {Card.MOVE1, Card.MOVE2, Card.AGAIN, Card.LEFT_TURN, Card.MOVE_BACK};
        server.expect(manyTimes(), requestTo(url + "games/123/round/sendRegisters")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(204)).headers(myheader));
        Boolean status = httpGameController.sendRegistersToMaster("123", cards);
        server.verify();
        Assert.assertEquals(true, status);
    }

    @Test
    public void sendRegistersToMasterFailed() throws Exception {
        HttpHeaders myheader = new HttpHeaders();
        myheader.set("secret", "secret");
        Card[] cards = {Card.MOVE1, Card.MOVE2, Card.AGAIN, Card.LEFT_TURN, Card.MOVE_BACK};
        server.expect(manyTimes(), requestTo(url + "games/123/round/sendRegisters")).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.valueOf(422)).headers(myheader));
        Boolean status = httpGameController.sendRegistersToMaster("123", cards);
        server.verify();
        Assert.assertEquals(false, status);
    }


}
package roboclient.controller.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import roboclient.controller.SecretHandler;
import roboclient.manager.GameManager;
import roboclient.model.Player;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RestGameController.class, secure = false)
public class RoundActionTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameManager manager;

    @Test
    public void roundActionSuccessfullyStarted() throws Exception {
        SecretHandler.setSecretKeyValue("secret");
        BDDMockito.given(this.manager.getMe()).willReturn(new Player());
        this.mvc.perform(post("/games/{id}/round/actions", "0000").header("secret", "secret")
                .content("{\n" +
                        "    \"position0\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 1,\n" +
                        "                \"y\": 2\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position1\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 6,\n" +
                        "                \"y\": 7\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position2\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 5,\n" +
                        "                \"y\": 6\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position3\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 7,\n" +
                        "                \"y\": 8\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position4\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 9,\n" +
                        "                \"y\": 10\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA122356\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 11,\n" +
                        "                \"y\": 12\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position5\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 13,\n" +
                        "                \"y\": 14\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA122356\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 15,\n" +
                        "                \"y\": 16\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action0\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action1\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action2\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action3\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action4\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action5\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }

    @Test
    public void roundActionWithWrongId() throws Exception {
        SecretHandler.setSecretKeyValue("secret");
        BDDMockito.given(this.manager.getMe()).willReturn(new Player());
        this.mvc.perform(post("/games/{id}/round/actions", "123").header("secret", "secret")
                .content("{\n" +
                        "    \"position0\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 1,\n" +
                        "                \"y\": 2\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position1\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 6,\n" +
                        "                \"y\": 7\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position2\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 5,\n" +
                        "                \"y\": 6\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position3\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 7,\n" +
                        "                \"y\": 8\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position4\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 9,\n" +
                        "                \"y\": 10\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA122356\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 11,\n" +
                        "                \"y\": 12\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"position5\" : [\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 13,\n" +
                        "                \"y\": 14\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"robot\": \"Hammer Bot\",\n" +
                        "            \"direction\": \"NORTH\",\n" +
                        "            \"playerName\": \"Mike\",\n" +
                        "            \"playerId\": \"ABGDGEGA122356\",\n" +
                        "            \"position\": {          \n" +
                        "                \"x\": 15,\n" +
                        "                \"y\": 16\n" +
                        "            },\n" +
                        "            \"checkpoints\" : [\"CHECK1\", \"CHECK2\"]\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action0\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action1\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action2\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action3\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action4\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ],\n" +
                        "    \"action5\" : [\n" +
                        "        {\n" +
                        "            \"playerId\": \"ABGDGEGA1256\",\n" +
                        "            \"action\": \"MOVE2\",\n" +
                        "            \"damageCards\": [\"SPAM\"]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"playerId\": \"ZZZDAD\",\n" +
                        "            \"action\": \"MOVE3\",\n" +
                        "            \"damageCards\": []\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }
}

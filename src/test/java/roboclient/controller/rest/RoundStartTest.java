package roboclient.controller.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import roboclient.controller.SecretHandler;
import roboclient.manager.GameManager;
import roboclient.model.Player;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RestGameController.class, secure = false)
public class RoundStartTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameManager manager;

    @Test
    public void roundStartSuccessfullyStarted() throws Exception {
        SecretHandler.setSecretKeyValue("secret");
        BDDMockito.given(this.manager.getMe()).willReturn(new Player());
        this.mvc.perform(post("/games/{id}/round/start", "0000").header("secret", "secret")
                .content("{\n" +
                        "\t\"playerPositions\": [{\n" +
                        "\t\t\"robot\": \"Hammer Bot\",\n" +
                        "\t\t\"direction\": \"NORTH\",\n" +
                        "\t\t\"playerName\": \"Mike\",\n" +
                        "\t\t\"playerId\": \"ABGDGEGA1256\",\n" +
                        "\t\t\"position\": {\n" +
                        "\t\t\t\"x\": 4,\n" +
                        "\t\t\t\"y\": 3\n" +
                        "\t\t}\n" +
                        "\t}],\n" +
                        "\t\"drawPile\": [\n" +
                        "\t\t\"MOVE1\", \"MOVE2\", \"UTURN\", \"SPAM\", \"LEFT_TURN\", \"RIGHT_TURN\", \"MOVE3\", \"MOVE_BACK\", \"AGAIN\"\n" +
                        "\t]\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void roundStartWithWrongID() throws Exception {
        SecretHandler.setSecretKeyValue("secret");
        BDDMockito.given(this.manager.getMe()).willReturn(new Player());
        this.mvc.perform(post("/games/{id}/round/start", 123).header("secret", "secret")
                .content("{\n" +
                        "\t\"playerPositions\": [{\n" +
                        "\t\t\"robot\": \"Hammer Bot\",\n" +
                        "\t\t\"direction\": \"NORTH\",\n" +
                        "\t\t\"playerName\": \"Mike\",\n" +
                        "\t\t\"playerId\": \"ABGDGEGA1256\",\n" +
                        "\t\t\"position\": {\n" +
                        "\t\t\t\"x\": 4,\n" +
                        "\t\t\t\"y\": 3\n" +
                        "\t\t}\n" +
                        "\t}],\n" +
                        "\t\"drawPile\": [\n" +
                        "\t\t\"MOVE1\", \"MOVE2\", \"UTURN\", \"SPAM\", \"LEFT_TURN\", \"RIGHT_TURN\", \"MOVE3\", \"MOVE_BACK\", \"AGAIN\"\n" +
                        "\t]\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }


}

package roboclient.controller.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import roboclient.controller.SecretHandler;
import roboclient.manager.GameManager;
import roboclient.model.Player;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(value = RestGameController.class, secure = false)
public class StartGameTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameManager manager;

    @Test
    public void startGameSuccessFull() throws Exception {
        SecretHandler.setSecretKeyValue("secret");
        BDDMockito.given(this.manager.getMe()).willReturn(new Player());
        this.mvc.perform(post("/games/{id}/start", "0000").header("secret", "secret")
                .content("{\n" +
                        "    \"board\": {\n" +
                        "        \"size\": {\n" +
                        "            \"width\": 10,\n" +
                        "            \"height\": 30\n" +
                        "        },\n" +
                        "        \"fields\": [\n" +
                        "            {\n" +
                        "                \"x\": 0,\n" +
                        "                \"y\": 0,\n" +
                        "                \"field\": \"EMPTY\"\n" +
                        "            },\n" +
                        "            {\n" +
                        "                \"x\": 1,\n" +
                        "                \"y\": 0,\n" +
                        "                \"field\": \"START\"\n" +
                        "            },\n" +
                        "            {\n" +
                        "                \"x\": 9,\n" +
                        "                \"y\": 29,\n" +
                        "                \"field\": \"PIT\"\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    },\n" +
                        "    \"availableRobots\": [\n" +
                        "        \"Hammer Bot\", \"Hulk X90\"\n" +
                        "    ],\n" +
                        "    \"availableStartingPositions\": [\n" +
                        "        {\n" +
                        "            \"x\": 1,\n" +
                        "            \"y\": 0\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"x\": 4,\n" +
                        "            \"y\": 3\n" +
                        "        }\n" +
                        "    ]\n" +
                        "} ")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void startGameWithWrongID() throws Exception {
        SecretHandler.setSecretKeyValue("secret");
        BDDMockito.given(this.manager.getMe()).willReturn(new Player());
        this.mvc.perform(post("/games/{id}/start", 124).header("secret", "secret")
                .content("{\n" +
                        "    \"board\": {\n" +
                        "        \"size\": {\n" +
                        "            \"width\": 10,\n" +
                        "            \"height\": 30\n" +
                        "        },\n" +
                        "        \"fields\": [\n" +
                        "            {\n" +
                        "                \"x\": 0,\n" +
                        "                \"y\": 0,\n" +
                        "                \"field\": \"EMPTY\"\n" +
                        "            },\n" +
                        "            {\n" +
                        "                \"x\": 1,\n" +
                        "                \"y\": 0,\n" +
                        "                \"field\": \"START\"\n" +
                        "            },\n" +
                        "            {\n" +
                        "                \"x\": 9,\n" +
                        "                \"y\": 29,\n" +
                        "                \"field\": \"PIT\"\n" +
                        "            }\n" +
                        "        ]\n" +
                        "    },\n" +
                        "    \"availableRobots\": [\n" +
                        "        \"Hammer Bot\", \"Hulk X90\"\n" +
                        "    ],\n" +
                        "    \"availableStartingPositions\": [\n" +
                        "        {\n" +
                        "            \"x\": 1,\n" +
                        "            \"y\": 0\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"x\": 4,\n" +
                        "            \"y\": 3\n" +
                        "        }\n" +
                        "    ]\n" +
                        "} ")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
package roboclient.controller;

import org.junit.Assert;
import org.junit.Test;
import roboclient.controller.http.post.data.GameCreateData;
import roboclient.controller.http.post.data.GameJoinData;
import roboclient.controller.http.post.data.SelectedCardData;
import roboclient.controller.http.response.GameCreateResponse;
import roboclient.controller.http.response.GameStatusResponse;
import roboclient.controller.rest.post.data.CardData;
import roboclient.controller.rest.post.data.PositionData;
import roboclient.controller.rest.response.StartGameResponse;
import roboclient.model.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;


public class ViewConverterTest {

    @Test
    public void convertCardDataArraytoCardArray() throws Exception {
        CardData[] testinputdata = new CardData[]{CardData.AGAIN, CardData.LEFT_TURN};
        Card[] testoutputdata = new Card[]{Card.AGAIN, Card.LEFT_TURN};
        Assert.assertThat(ViewConverter.convertCardDataArraytoCardArray(testinputdata), equalTo(testoutputdata));
    }

    @Test
    public void convertCardArrayToCardDataArray() throws Exception {
        Card[] testinputdata = new Card[]{Card.AGAIN, Card.LEFT_TURN};
        SelectedCardData[] testoutputdata = new SelectedCardData[]{SelectedCardData.AGAIN, SelectedCardData.LEFT_TURN};
        Assert.assertThat(ViewConverter.convertCardArrayToCardDataArray(testinputdata), equalTo(testoutputdata));
    }

    @Test
    public void convertGametoGameCreatePost() throws Exception {
        GameCreateData testoutputdata = new GameCreateData("Testgame", 5);
        Game testinputdata = new Game("Testgame", 5);
        Assert.assertThat(ViewConverter.convertGametoGameCreatePost(testinputdata).getMaxRobotCount(), equalTo(testoutputdata.getMaxRobotCount()));
        Assert.assertThat(ViewConverter.convertGametoGameCreatePost(testinputdata).getName(), equalTo(testoutputdata.getName()));
    }

    @Test
    public void convertCardDataArraytoCardArray1() throws Exception {
        CardData[] testinputdata = new CardData[]{CardData.MOVE1, CardData.AGAIN};
        Card[] testoutputdata = new Card[]{Card.MOVE1, Card.AGAIN};
        Assert.assertThat(ViewConverter.convertCardDataArraytoCardArray(testinputdata), equalTo(testoutputdata));
    }

    @Test
    public void convertPlayertoStartGameResponse() throws Exception {
        Player testinputdata = new Player(new Robot("Hammer Bot"), new Position(4, 5));
        StartGameResponse testoutputdata = new StartGameResponse("Hammer Bot", new PositionData(4, 5));
        Assert.assertThat(ViewConverter.convertPlayertoStartGameResponse(testinputdata).getPosition().getX(), equalTo(testoutputdata.getPosition().getX()));
        Assert.assertThat(ViewConverter.convertPlayertoStartGameResponse(testinputdata).getPosition().getY(), equalTo(testoutputdata.getPosition().getY()));
        Assert.assertThat(ViewConverter.convertPlayertoStartGameResponse(testinputdata).getRobot(), equalTo(testoutputdata.getRobot()));
    }

    @Test
    public void convertGameCreateResponsetoGame() throws Exception {
        GameCreateResponse testinputdata = new GameCreateResponse("123", "testgame", 4, 0);
        Game testoutputdata = new Game("testgame", 4, 0, "123");
        Assert.assertThat(ViewConverter.convertGameCreateResponsetoGame(testinputdata).getName(), equalTo(testoutputdata.getName()));
        Assert.assertThat(ViewConverter.convertGameCreateResponsetoGame(testinputdata).getId(), equalTo(testoutputdata.getId()));
    }

    @Test
    public void convertGameStatusResponsetoGame() throws Exception {
        GameStatusResponse testinputdata = new GameStatusResponse("ABC", "testgame", 5, 0, "NOT_STARTED", new ArrayList<>());
        Game testoutputdata = new Game("testgame", 5, 0, "ABC", "NOT_STARTED", new ArrayList<>());
        Assert.assertThat(ViewConverter.convertGameStatusResponsetoGame(testinputdata).getId(), equalTo(testoutputdata.getId()));
        Assert.assertThat(ViewConverter.convertGameStatusResponsetoGame(testinputdata).getName(), equalTo(testoutputdata.getName()));
        Assert.assertThat(ViewConverter.convertGameStatusResponsetoGame(testinputdata).getCurrentRoboCount(), equalTo(testoutputdata.getCurrentRoboCount()));
        Assert.assertThat(ViewConverter.convertGameStatusResponsetoGame(testinputdata).getMaxRoboCount(), equalTo(testoutputdata.getMaxRoboCount()));
        Assert.assertThat(ViewConverter.convertGameStatusResponsetoGame(testinputdata).getStatus(), equalTo(testoutputdata.getStatus()));
    }

    @Test
    public void convertPlayertoGameJoinPost() throws Exception {
        Player testinputdata = new Player("Hans", "http://127.0.0.1");
        GameJoinData testouputdata = new GameJoinData("Hans", "http://127.0.0.1");
        Assert.assertThat(ViewConverter.convertPlayertoGameJoinPost(testinputdata).getPlayerName(), equalTo(testouputdata.getPlayerName()));
        Assert.assertThat(ViewConverter.convertPlayertoGameJoinPost(testinputdata).getPlayerNodeRestBaseUrl(), equalTo(testouputdata.getPlayerNodeRestBaseUrl()));
    }

    @Test
    public void convertPositionDataListToPositionList() throws Exception {
        List<PositionData> testinputdata = new ArrayList<>();
        List<Position> testoutputdata = new ArrayList<>();
        testinputdata.add(new PositionData(5, 3));
        testoutputdata.add(new Position(5, 3));
        Assert.assertThat(ViewConverter.convertPositionDataListToPositionList(testinputdata), equalTo(testoutputdata));
    }

}
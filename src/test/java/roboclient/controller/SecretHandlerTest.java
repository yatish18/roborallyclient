package roboclient.controller;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.internal.matchers.Any;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import roboclient.controller.http.SecretNotFoundException;

import static org.hamcrest.Matchers.*;

public class SecretHandlerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void extractSecret() throws Exception {
        SecretHandler.setSecretKeyValue("testsecret");
        Assert.assertThat(SecretHandler.extractSecret(SecretHandler.addSecretResponse(new ResponseEntity<Any>(HttpStatus.ACCEPTED))), equalTo("testsecret"));
    }

   @Test
    public void addSecret() throws Exception {
        SecretHandler.setSecretKeyValue("testsecret");
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> testinputdata = new HttpEntity<>("helloWorld", headers);
        Assert.assertThat(SecretHandler.addSecret(testinputdata).getHeaders().get("secret").get(0), is("testsecret"));
   }

    @Test
    public void addSecretWithoutBody() throws Exception {
        SecretHandler.setSecretKeyValue("testsecret");
        Assert.assertThat(SecretHandler.addSecret(new HttpEntity<>(HttpStatus.OK)).getHeaders().get("secret").get(0), is("testsecret"));
    }

    @Test
    public void validateSecretOk() throws Exception {
        SecretHandler.setSecretKeyValue("testsecret");
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> testinputdata = new HttpEntity<>("helloWorld", headers);
        testinputdata = SecretHandler.addSecret(testinputdata);
        Assert.assertThat(SecretHandler.validateSecret(testinputdata.getHeaders()), equalTo(true));
    }

    @Test
    public void validateSecretWrong() throws Exception {
        SecretHandler.setSecretKeyValue("testsecret");
        HttpHeaders headers = new HttpHeaders();
        headers.set(SecretHandler.getSECRETKEY(), "falsesecret");
        HttpEntity<String> testinputdata = new HttpEntity<>("helloWorld", headers);
        thrown.expect(SecretNotFoundException.class);
        SecretHandler.validateSecret(testinputdata.getHeaders());
    }

    @Test
    public void addSecretResponse() throws Exception {
        //any(HttpStatus.class) not working :( ? Why ask in Class...
        Assert.assertThat(SecretHandler.extractSecret(SecretHandler.addSecretResponse(new ResponseEntity<Any>(HttpStatus.ACCEPTED))), equalTo(SecretHandler.getSecretKeyValue()));
    }

}
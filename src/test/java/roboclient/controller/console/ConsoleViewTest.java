package roboclient.controller.console;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import roboclient.manager.GameManager;
import roboclient.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleViewTest {


    @InjectMocks
    private ConsoleView myview;

    @Mock
    private GameManager mymanager;


    @Test
    public void dropPileValidator() throws Exception {
        String selectedOption = "1,2,3,4,5";
        Mockito.when(this.mymanager.getMe()).thenReturn(new Player());
        Optional<int[]> data = this.myview.dropPileValidator(selectedOption);
        Assert.assertEquals(5, data.get().length);
    }


    @Test
    public void printGameStart() throws Exception {
        Mockito.when(this.mymanager.getMe()).thenReturn(new Player(new Robot("Hammer Bot"), new Position(6, 4)));
        String testoutputdata = "Game started... \n" + "-----------------------------------------------\n" +
                "Bot: Hammer Bot\n" +
                "Start Position \nX: 6\nY: 4\n" + "-----------------------------------------------\n";
        Assert.assertThat(this.myview.printGameStart(), equalTo(testoutputdata));
    }

    @Test
    public void printCurrentStateWithoutCheckpoints() throws Exception {
        List<Checkpoint> checkpoint_mock = new ArrayList<>();
        State mockdata = new State("ABC", checkpoint_mock, new Position(4, 5), new Robot("hulk"));
        String testoutputdata = "Round successfully completed!\n" +
                "Your current position is the following:\n" +
                "-----------------------------------------------\n" +
                "Position X: 4\n" +
                "Position Y: 5\n" +
                "-----------------------------------------------\n";
        Mockito.when(this.mymanager.getCurrentGameState()).thenReturn(mockdata);
        Assert.assertThat(this.myview.printCurrentState(), equalTo(testoutputdata));
    }

    @Test
    public void printCurrentStateWithCheckpoints() throws Exception {
        List<Checkpoint> checkpoint_mock = new ArrayList<>();
        checkpoint_mock.add(Checkpoint.CHECK1);
        checkpoint_mock.add(Checkpoint.CHECK2);
        State mockdata = new State("ABC", checkpoint_mock, new Position(4, 5), new Robot("hulk"));
        String testoutputdata = "Round successfully completed!\n" +
                "Your current position is the following:\n" +
                "-----------------------------------------------\n" +
                "Position X: 4\n" +
                "Position Y: 5\n" +
                "-----------------------------------------------\n" +
                "You already reached the following Checkpoints: \n" +
                "CHECK1 CHECK2 ";
        Mockito.when(this.mymanager.getCurrentGameState()).thenReturn(mockdata);
        Assert.assertThat(this.myview.printCurrentState(), equalTo(testoutputdata));
    }


    @Test
    public void consolePrinter() throws Exception {
        Assert.assertThat(myview.consolePrinter("test"), equalTo(true));
    }

    @Test
    public void printDrawPileHeader() throws Exception {
        String testoutputdata =
                "Please Input Index of Cards in movement order:\n" +
                        "-----------------------------------------------\n" +
                        "Please enter exactly 5 Cards!\n" +
                        "Each Card can only used once!\n" +
                        "Seperate Card Indexes by using commas[,]\n" +
                        "-----------------------------------------------\n";
        Assert.assertThat(myview.printDrawPileHeader(), equalTo(testoutputdata));
    }

    @Test
    public void printDrawPile() throws Exception {
        Card[] mockdata = new Card[]{Card.MOVE2, Card.MOVE3};
        Mockito.when(this.mymanager.getCurrentDrawPile()).thenReturn(mockdata);
        String testoutputdata = "\nCurrent Draw Pile:\n" +
                "-----------------------------------------------\n" +
                "[0] MOVE2\n" +
                "[1] MOVE3\n" +
                "-----------------------------------------------\n";
        Assert.assertThat(myview.printDrawPile(), equalTo(testoutputdata));
    }


    @Test
    public void exitGameWrongValue() throws Exception {
        Assert.assertThat(this.myview.exitGame("hello"), CoreMatchers.is(true));
    }

    @Test
    public void exitGameOkValue() throws Exception {
        Mockito.when(this.mymanager.exitGame()).thenReturn(true);
        Assert.assertThat(this.myview.exitGame("Q"), CoreMatchers.is(false));
    }

    @Test
    public void printMenu() throws Exception {
        String testoutputdata = "Please select any option below\n" +
                "1.Create New Game\n" +
                "2.List of Games\n" +
                "3.Join Game\n" +
                "4.Exit Game\n" +
                "-----------------------------------------------\n";
        Assert.assertThat(this.myview.printMenu(), equalTo(testoutputdata));
    }
}